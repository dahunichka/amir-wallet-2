import { Transport, ClientOptions, ClientsModuleOptions } from "@nestjs/microservices";
import { join, dirname } from "path";

const protoModule = require.resolve('proto-schema');

console.log('***', dirname(protoModule) + '/account.proto');

export const getGRPC = (): ClientOptions => ({
	transport: Transport.GRPC,
	options: {
		package: 'account',
		protoPath: join(dirname(protoModule) + '/proto/account.proto'),
	},
})

export const getGRPCClientConfig = (): ClientsModuleOptions => ([
	{ name: "AUTH_PACKAGE", ...getGRPC() }
]);
