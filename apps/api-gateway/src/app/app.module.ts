import { getGRPCClientConfig } from './configs/grpc.config';
import { ApolloDriverConfig } from '@nestjs/apollo';
import { getGraphQL } from './configs/graphql.config';
import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';
import { AccountController } from './controllers/account.controller';
import { GraphQLModule } from '@nestjs/graphql';
import { AccountResolver } from './controllers/account.resolver';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: 'src/environments/.api.env' }),
    //GraphQLModule.forRoot<ApolloDriverConfig>(getGraphQL()),
    ClientsModule.register(getGRPCClientConfig())],
  controllers: [AccountController],
  providers: [/* AccountResolver */],
})
export class AppModule { }
